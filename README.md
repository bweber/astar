Astar
=============

Aby uruchomić aplikację należy posiadać na komuterze zainstalowany RVM oraz Ruby w wersji 2.0.0
Następnie należy zainstalować gem [rubygame](http://rubygame.org/wiki/Linux_Installation_Guide).

W celu uruchomienia wpisujemy w terminalu:

	ruby astargame.rb

Program implementujący heurystyczny algorytm A*

Program został napisany za pomocą języka Ruby. Został wykorzystane gem Rubygame pozwalający na rozszeżenie języka o funkcję pozwalające na stworzenie prostych gier.

Gemfile:
```Ruby
source "https://rubygems.org"
ruby '2.0.0'


gem 'rubygame'
```

Program działa na macierzy w kórej wyznaczone zostają punkt początkowy i końcowy ścieżki. Algorytm A* ma za zadanie odnalezienia ścieżki z jednego punktu do drugirgo.

Mamy możliwość tworzenia przeszkód lub jak kto woli ścian, które nasza ścieżka musi ominąć by dostać się do punktu końcowego.

Implementacja wzoru obliczajacego koszt ruchu w algorytmie A*:

```Ruby
	def calc_cost(dest_x, dest_y)
		@cost = Math.hypot(x-dest_x, y-dest_y).round(2) # Oblicz przeciw prostokątną, zaokrąglij do dwój miejsc po przecinku
	end
```
