
require "rubygems"
require "rubygame"

include Rubygame

# Dokumentacja:
# http://www.ruby-doc.org/core-2.1.2/Math.html
# http://docs.rubygame.org/rdoc/2.5.0/classes/Rubygame/Clock.html
# http://docs.rubygame.org/rdoc/2.5.0/classes/Rubygame/Screen.html
# http://docs.rubygame.org/rdoc/2.6.2/classes/Rubygame/EventQueue.html

class Node
	attr_accessor :x, :y, :parent, :walkable, :cost
	def initialize(x,y, walkable)
		@x = x
		@y = y
		@walkable = walkable
		@parent = nil
		@cost = nil
	end

	def calc_cost(dest_x, dest_y)
		@cost = Math.hypot(x-dest_x, y-dest_y).round(2) # Oblicz przeciw prostokątną, zaokrąglij do dwój miejsc po przecinku
	end
end



class Game
	def initialize
		@map_width = 30 # Szerokość mapy
		@map_height = 30 # Wysokość mapy
		@map=[] # Tablica mapy

		# Pętla wypełniająca tablicę mapy, wszędzie 0, można przemieszczać się po całej mapie
		(0...@map_width).each do |x|
			(0...@map_height).each do |y|
				@map[y]=[] if @map[y].nil?
				@map[y][x] = 0
			end # koniec pętli zagnieżdżonej
		end # koniec pętli

		# Punkt początkowy i końcowy ścieżki
		@start_point = [4,5] # Punkt startowy
		@end_point = [24, 21] # Punkt końcowy

		nodes_map # stwórz mapę pozycji
		@mypath = find # Wyznacz ścieżkę

		@screen = Screen.new [800,600], 0, [HWSURFACE, DOUBLEBUF] # Stwórz nowe okno
		@cell_width = @screen.size[0]/@map_width # Szerokość komórki
		@cell_height = @screen.size[1]/@map_height # Wysokość komórki
		@crad = @cell_height/2 # zmienna obliczająca średnicę komórki (w celu narysowania okręgu)
		@queue = EventQueue.new # obiekt zarządząjący zdarzeniami, zbiór metod związanych z zdarzeniami
		@clock = Clock.new # Zegar, zbór metod związanych z czasem
		@clock.target_framerate = 30 # ustaw 30 klatek na sekundę
	end

	# Metoda odnajdująca ścieżkę
	def find
		open_list = [] # Lista otwartych węzłów
		close_list = [] # Lista zamkniętych węzłów

		start_node =  @nodes_map[@start_point[0]][@start_point[1]] # Początkowy węzeł
		start_node.calc_cost(@end_point[0], @end_point[1]) # Oblicz koszt

		end_node =  @nodes_map[@end_point[0]][@end_point[1]] # końcowy węzeł

		open_list << start_node

		while(open_list.size > 0)
			p = nil
			open_list.each do |e|
				if p.nil?
					p = e
				else
					p = e if e.cost < p.cost 
				end # koniec if
			end # koniec pętli

			open_list.delete(p)
			close_list << p

			# sprawdzanie czy już dotarł do końca
			if p.x == @end_point[0] and p.y == @end_point[1]
				break
			end # koniec if

			#możliwe ruchy
			moves = []
			moves << @nodes_map[p.x + 1][p.y]
			moves << @nodes_map[p.x - 1][p.y]
			moves << @nodes_map[p.x][p.y + 1]
			moves << @nodes_map[p.x][p.y - 1]
			moves << @nodes_map[p.x + 1][p.y + 1]
			moves << @nodes_map[p.x - 1][p.y - 1]
			moves << @nodes_map[p.x - 1][p.y + 1]
			moves << @nodes_map[p.x + 1][p.y - 1]

			moves.each do |m|
				unless m.nil?
					if m.walkable and not close_list.include?(m)
						unless open_list.include?(m)
							m.parent = p
							m.calc_cost(@end_point[0], @end_point[1])
							open_list << m
						end # koniec unless
					end # koniec if
				end # koniec unless
			end # koniec pętli
		end # koniec find

		path = [] # przypisz pustą tablicę
		p = end_node # przypisz węzeł końcowy
		while(p != nil) # jeżeli wezeł końcowy != obiekt pusty
			path.push p
			p = p.parent
		end # koniec while
		return path # zwróć ścieżkę
	end # koniec find
	
	def nodes_map # mapa pozycji
		@nodes_map = [] # pusta tablica
		(0...@map_width).each do |x|
			@nodes_map[x] = []
			
			(0...@map_height).each do |y|
				walkable = @map[y][x] == 0
				@nodes_map[x][y] = Node.new(x, y, walkable)
			end # koniec pętli zagnieżdżonej
		end # koniec pętli
	end # koniec nodes_map

	def map_position coord
		cx = coord[0]
		cy = coord[1]

		(0...@map_width).each do |x|
			(0...@map_height).each do |y|
				x_pos = x * @cell_width
				y_pos = y * @cell_height
				r = Rect.new(x_pos, y_pos, @cell_width, @cell_height)
				if r.collide_point? cx, cy
					return [x, y]
				end # koniec if
			end # koniec pętli zagnieżdżonej
		end # koniec pętli
		return nil # zwróc pusty obiekt
	end # koniec map_position

	def update
		@queue.each do |ev|
			case ev
			when QuitEvent
				Rubygame.quit # Wciśnięcie x na oknie
				exit # wyjdź z programu
			when MouseDownEvent
				map_point = map_position ev.pos # przypisz pozycję kliknięcia
				unless map_point.nil?
					ix = map_point[0]
					iy = map_point[1]
					val = @map[iy][ix]

					if val == 1
						@map[iy][ix] = 0
					else
						@map[iy][ix] = 1
					end # koniec if

					# POPRAWKA Z ZAJĘĆ
					# Zablokowanie stawiania ściany na punkcie startowym
					if @start_point[0] == ix and @start_point[1] == iy
						@map[iy][ix] = 0
					end

					# Zablokowanie stawiania ściany na punkcie końcowym
					if @end_point[0] == ix and @end_point[1] == iy
						@map[iy][ix] = 0
					end
					# KONIEC POPRAWKI Z ZAJĘĆ
					
					nodes_map
					@mypath = find
				end # koniec unless
			end # koniec when
		end # koniec pętli
	end # koniec update

	def draw
		@screen.fill BACKGROUND_COLOR # Wypełnij ekran tłem
		last_node = nil

		# rysowanie mapy
		(0...@map_width).each do |x|
			(0...@map_height).each do |y|
				x_pos = x * @cell_width
				y_pos = y * @cell_height
				r = Rect.new(x_pos, y_pos, @cell_width, @cell_height)
				node = @nodes_map[x][y]
				c = r.center()

				unless node.walkable # dopuki pole jest dostępne
					@screen.draw_circle_s(c, @crad, 'blue') # rysuj niebieskie kółko na ekranie (ścianka)
				end # koniec unless
			end # koniec pętli zagnieżdżonej
		end # koniec pętli

		last_pos = nil

		for n in @mypath
			x_pos = n.x * @cell_width
			y_pos = n.y * @cell_height
			r = Rect.new(x_pos, y_pos, @cell_width, @cell_height) # stwórz nowy prostokąt
			c = r.center() # Oblicz środek prostokąta
			if last_pos.nil? # jeżeli last_pos jest obiektem pustym
				last_pos = c # przypisz środek prostokąta
			else
				@screen.draw_line_a(last_pos, c, 'red') # rysuj czerwoną linię
				last_pos = c # przypisz środek prostokąta do ostatniej pozycji
			end # koniec if
		end

		@screen.update # uaktualnij ekran
	end #koniec draw

	BACKGROUND_COLOR = 'white' # Kolor tła

	def run # metoda uruchamiajaca grę
		loop do # nieskończona pętla
			update
			draw
			@clock.tick
		end # koniec pętli
	end # koniec run

end # koniec klasy Game

game = Game.new # Utwórz obiekt
game.run # Uruchom metodę run